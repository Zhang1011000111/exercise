const express = require('express')
const app = express()
const path = require('path')
// const qs = require('querystring')
// const data={
//     name:'小明',
//     age:18,
//     sex:"男"
// }
//获取请求体中间件
app.use(express.urlencoded())
app.use(express.json())
//手写请求体中间件
// function getRequestBody(req,res,next){
//     let dataStr=''
//     req.on('data',(err,chunk)=>{
//         dataStr+=chunk
//     })
//     req.on('end',()=>{
//        qs.parse(dataStr)
//     })
// }


//id查询接口(query)
app.get('/user',(request,response)=>{
    const {
        userId
    }=request.query
    if(userId!=="001"){
        return response.send(
            {
                code:10001,
                msg:"查询失败",
                data:"null"
            }
        )
    }
    response.send(
        {
            code:10000,
            msg:"查询成功",
            data:data
        }
    )
})

//id查询接口(path)
app.get('/user/:userId',(request,response)=>{
    const {
        userId
    }=request.params
    console.log(userId);
    if(userId!=="001"){
        return response.send(
            {
                code:10001,
                msg:"查询失败",
                data:"null"
            }
        )
    }
    response.send(
        {
            code:10000,
            msg:"查询成功",
            data:data
        }
    )
})
//设置用户信息接口
app.post('/adduser',(request,response)=>{
   
   const{
       name,
       age,
       sex
   }=request.body
    
    if(!name||!age||!sex){
        return response.send(
            {
                code:10001,
                msg:"查询失败",
                data:"null"
            }
        )
    }
    response.send(
        {
            code:10000,
            msg:"查询成功",
            data:"null"
        }
    )
})


//静态文件部署服务器
app.get('/1',(request,response)=>{
    const filePath = path.resolve(__dirname,'./01.基础的get请求.html')
    response.sendFile(filePath)
})
app.get('/2',(request,response)=>{
    const filePath = path.resolve(__dirname,'./02.接口请求query方法.html')
    response.sendFile(filePath)
})
app.get('/3',(request,response)=>{
    const filePath = path.resolve(__dirname,'./03.接口请求path方法.html')
    response.sendFile(filePath)
})
app.get('/4',(request,response)=>{
    const filePath = path.resolve(__dirname,'./04.post请求.html')
    response.sendFile(filePath)
})


app.listen('8090',()=>{
    console.log(111);
})