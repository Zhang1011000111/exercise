function ajax(url,callback){
    let xml = new XMLHttpRequest()
    xml.open('get',url)
    xml.send()
    xml.onreadystatechange=function(){
        if(xml.readyState===4){
            callback(JSON.parse(this.responseText))
        }
        
    }
}
