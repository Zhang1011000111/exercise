function ajax(url){
    return new Promise((resolve,reject)=>{
        let xml = new XMLHttpRequest()
    xml.open('get',url)
    xml.send()
    xml.onreadystatechange=function(){
            //请求状态成功,服务器返回成功  双重判定进入语句
            //放入异步执行,等待同步状态成功再执行异步一样可以
            setTimeout(() => {
                if(xml.readyState===4&&xml.status===200){
                    // console.log(xml.responseText);
                    resolve(JSON.parse(xml.responseText))
                }else{
                    reject('错误')
                }
            },1000);
        
    }
    xml.onerror=function(){
        reject('错误')
    }
    })
    
}
