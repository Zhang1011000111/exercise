const configProd  = require("./webpack.config.prod.js")
module.exports={
    ...configProd,
    devServer: {
        port: 8090, // 端口号
        open: true,  // 自动打开浏览器
        compress: true, //启动gzip压缩
        liveReload:true,//启动自动更新
      },
      mode:"development",
}