const path = require("path")
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin")
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin')
const CopyWebpackPlugin = require("copy-webpack-plugin")
module.exports = {
    entry: path.resolve(__dirname + './../src/index.js'),
    output: {
        path: path.resolve(__dirname + './../bundel'),
        filename: "./js/build.js"
    },
    module: {
        rules: [{
                test: /\.css$/i,
                //   use: ["style-loader", "css-loader"],
                use: [MiniCssExtractPlugin.loader, "css-loader"],

            },
            {
                test: /\.less$/i,
                // use: ["style-loader", "css-loader", "less-loader"],
                use: [MiniCssExtractPlugin.loader, "css-loader", "less-loader"],

            },
            {
                test: /\.html$/i,
                loader: "html-loader",
            },
            {
                test: /\.(png|jpg|gif|webg|jpeg)$/,
                //asset 在导出一个 data URI 和发送一个单独的文件之间自动选择。代替之前的url-loader和file-loader
                type: "asset",
                parser: {
                    dataUrlCondition: {
                        maxSize: 30 * 1024, // 小于15kb以下的图片会被打包成base64格式
                    },
                },
                generator: {
                    //参照出口文件位置
                    filename: 'img/[name][ext]'
                }
            },
        ],
    },
    optimization: {
        minimizer: [
            new CssMinimizerPlugin(),
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "./css/main.css"
        }),
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: path.resolve(__dirname, "../public/index.html"),
        }),
        new ESLintPlugin({
            fix: true
        }),
        new CopyWebpackPlugin({
            patterns: [{
              from: path.resolve(__dirname, "../public"),
              to: "public",
              //配置复制时忽略的文件
              globOptions: {
                ignore: ["**/index.html"]
              }
            }],
          }),
    ],
    mode: "production"
}