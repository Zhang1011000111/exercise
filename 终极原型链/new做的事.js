/* 
1 声明一个对象 
2 将对象的隐式原型指向构造函数的显示原型
3 调用构造函数 将this指向改为这个对象,为对象添加属性
4 返回这个对象
*/
function Fn(){
    
}
const obj = new Fn
// obj = {}
// obj.__proto__=Fn.prototype
//obj = Fn.call(obj)

